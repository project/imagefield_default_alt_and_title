/**
 * @file
 * JavaScript behaviors for Imagefield default alt and title.
 */

(function ($, Drupal, once) {

  'use strict';

  /**
   * Init Imagefield default alt and title.
   *
   * @type {Drupal~behavior}
   *
   * @prop {Drupal~behaviorAttach} attach
   *   Attaches the behavior for the Imagefield default alt and title.
   */
  Drupal.behaviors.initImgAltTitle = {
    attach(context) {
      // Set default status and default value.
      once(
        'initImgAltTitle',
        '.image-widget input.form-text, .image-widget-data input.form-text',
        context,
      ).forEach((e) => {
        const $e = $(e);
        const titleVal = $('.field--name-title input, .field--name-name input').val();
        $e.data('imgAltTitle', {
          needAdd: !$e.val()
        });

        // Set default value if need.
        if (titleVal && $e.data('imgAltTitle')['needAdd']) {
          $e.val(titleVal);
        }
      });

      // Switch status.
      $(context).find('.image-widget input.form-text, .image-widget-data input.form-text').on('change.imgAltTitle', (e) => {
        $(e.target).data('imgAltTitle', {
          needAdd: false
        });
      });

      // Set value.
      $(context).find('.field--name-title input, .field--name-name input').on('change.imgAltTitle', (e) => {
        $('.image-widget input.form-text, .image-widget-data input.form-text').each(() => {
          const $this = $(this);
          if ($this.data('imgAltTitle')['needAdd']) {
            $this.val($(e.target).val());
          }
        });
      });
    }
  };

})(jQuery, Drupal, once);
